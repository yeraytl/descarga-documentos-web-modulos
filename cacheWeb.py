import urllib.request


class Robot:
    def __init__(self, url):
        self.url = url
        self.retrieved = False
        print(self.url)

    def retrieve(self):
        if not self.retrieved:
            print(f"Descargando {self.url}")
            with urllib.request.urlopen(self.url) as response:
                self.content = response.read().decode('utf-8')
                self.retrieved = True

    def show(self):
        print(self.content())

    def content(self):
        self.retrieve()
        return self.content


class Cache:
    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        if url not in self.cache:
            self.cache[url] = Robot(url)

    def show(self, url):
        print(self.content(url))

    def show_all(self):
        for url in self.cache:
            if self.cache[url].content is not None:
                print(url)

    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()


if __name__ == '__main__':

    print("Test Clase Robot")
    robot1 = Robot('http://gsyc.urjc.es/')
    robot1.show()
    robot1.retrieve()
    robot1.retrieve()

    print("Test Clase Cache")

    cache1 = Cache()
    cache1.retrieve('http://gsyc.urjc.es/')
    cache1.show('https://www.aulavirtual.urjc.es')
    cache1.show_all()
